using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamUpdate:MonoBehaviour {
    public LineRenderer mLine;

    private void Start() {
        enabled = false;
    }

    public void SetPositions(Vector3 start,Vector3 end) {
        mLine.SetPosition(0,start);
        mLine.SetPosition(1,end);
    }
}