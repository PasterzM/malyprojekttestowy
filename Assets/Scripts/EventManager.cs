using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager:MonoBehaviour {
    public static EventManager mInstance;
    GameObject mInterObj;

    public delegate void InteractExecMEthods(EventTypes status,GameObject obj);

    Dictionary<EventTypes,InteractExecMEthods> mDelegates;

    

    public enum EventTypes {
        ENTER,
        EXCECUTE,
        END,
        EXIT,
        ENDGAME
    }


    public void AddEvent(EventTypes state,InteractExecMEthods method) {
        if(!mDelegates.ContainsKey(state)) {
            mDelegates.Add(state,method);
        } else {
            mDelegates[state] += method;
        }
    }

    public void SetInteractObj(GameObject inter) { mInterObj = inter; }

    private void Awake() {
        mInstance = this;
        mDelegates = new Dictionary<EventTypes,InteractExecMEthods>();
    }

    public void ExcecuteEvent(EventTypes status) { mDelegates[status]?.Invoke(status,mInterObj); }
}
