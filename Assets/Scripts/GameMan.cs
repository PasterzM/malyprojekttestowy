using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameMan:MonoBehaviour {

    public static GameMan mInstance;

    private void Start() {
        mInstance = this;
    }

    public enum GameState {
        PLAY,
        PAUSE,
        END,
    }

    public void LoadScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }

    public void ExitGame() {
        Application.Quit();
    }
}
