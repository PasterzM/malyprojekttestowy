using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager:MonoBehaviour {

    class GameOverPanel {
        public Text mText;
        public Image mPanel;
    }

    public static CanvasManager mInstance;

    public GameObject mEventExcPanelInf;
    public GameObject mGameOverPanel;

    bool isGameOver;
    GameOverPanel mGameOver;

    // Start is called before the first frame update
    void Start() {
        if(mInstance == null)
            mInstance = this;
        EventManager.mInstance.AddEvent(EventManager.EventTypes.ENTER,ShowInteractionButton);
        EventManager.mInstance.AddEvent(EventManager.EventTypes.ENDGAME,EndGame);
        EventManager.mInstance.AddEvent(EventManager.EventTypes.EXIT,ShowInteractionButton);

        mGameOver = new GameOverPanel();
        mGameOver.mText = mGameOverPanel.GetComponentInChildren<Text>();
        mGameOver.mPanel = mGameOverPanel.GetComponentInChildren<Image>();

    }

    private void EndGame(EventManager.EventTypes status,GameObject obj) {
        enabled = true;
        isGameOver = true;
        mGameOver.mPanel.gameObject.SetActive(true);
    }

    public void ShowInteractionButton(EventManager.EventTypes state,GameObject obj) {
        if(state == EventManager.EventTypes.ENTER)
            mEventExcPanelInf.SetActive(true);
        else
            mEventExcPanelInf.SetActive(false);
    }

    private void Update() {
        if(isGameOver && mGameOver.mText.fontSize < 300) {
            mGameOver.mText.fontSize += 1;
        }
    }
}
