﻿using UnityEngine;

public class MoveObstacleInteraction:MonoBehaviour {
    public BeamUpdate mPowerBeamPref;
    BeamUpdate mBeam;
    Transform mBeamStart;
    public Transform BeamPoint;

    //public PostPrecessVolume mPostProcess;

    ForceMovingObstacle mForce;

    private void Start() {
        enabled = false;
        EventManager.mInstance.AddEvent(EventManager.EventTypes.EXCECUTE,ExcecuteEvent);
        EventManager.mInstance.AddEvent(EventManager.EventTypes.END,EndEvent);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.tag == "Player") {
            mBeamStart = collision.transform.Find("BeamPoint");
            EventManager.mInstance.SetInteractObj(gameObject);
            EventManager.mInstance.ExcecuteEvent(EventManager.EventTypes.ENTER);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if(collision.tag == "Player") {
            EventManager.mInstance.SetInteractObj(null);
            EventManager.mInstance.ExcecuteEvent(EventManager.EventTypes.EXIT);
            if(mBeam == null)
                Cursor.visible = false;
        }
    }

    public void ExcecuteEvent(EventManager.EventTypes status,GameObject inter) {
        if(gameObject == inter) {
            mBeam = Instantiate(mPowerBeamPref,this.transform);
            mBeam.SetPositions(mBeamStart.position,BeamPoint.position);
            enabled = true;
            Cursor.visible = true;
            mForce = new ForceMovingObstacle(Input.mousePosition,GetComponent<Rigidbody2D>());
            //uruchom post proces
        }
    }

    public void EndEvent(EventManager.EventTypes status,GameObject inter) {
        if(mBeam != null) {
            mBeam.transform.SetParent(null);
            GameObject.Destroy(mBeam.gameObject);
            Cursor.visible = false;
            //wylacz post proces
        }
    }

    private void Update() {
        if(mBeam != null) {
            mBeam.SetPositions(mBeamStart.position,BeamPoint.position);
            mForce.Update(Input.mousePosition);
        }
    }
}