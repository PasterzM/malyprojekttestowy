using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player:MonoBehaviour {
    [Header("Camera")]
    [SerializeField] Transform mCamera;
    [SerializeField] Vector3 mCamOffset;
    [SerializeField] [Range(.1f,5f)] float mCamSpeed;

    [Header("Character graphic")]
    [SerializeField] Transform mCharPosses; //posta�, kt�r� steruje gracz
    [SerializeField] Vector2 mCharScale;
    Animator mPossesCharAnimat;

    [Header("Movement")]
    //klawisze odpowiadaj�ce za ruch postaci
    [SerializeField] KeyCode mRight;
    [SerializeField] KeyCode mLeft;
    [SerializeField] float mSpeed;
    Vector3 mShift;
    Vector2 mTurnLeft;


    bool isIntButtReq;
    GameMan.GameState mActGameState;

    private void Start() {
        mShift = Vector3.zero;
        mActGameState = GameMan.GameState.PLAY;
        mTurnLeft = new Vector2(-1f,1f);
        mPossesCharAnimat = mCharPosses.GetComponent<Animator>();
        mCharPosses.tag = "Player";
        isIntButtReq = false;
        EventManager.mInstance.AddEvent(EventManager.EventTypes.ENTER,EnableEButton);
        EventManager.mInstance.AddEvent(EventManager.EventTypes.EXIT,DisableEButton);
        EventManager.mInstance.AddEvent(EventManager.EventTypes.EXCECUTE,DisableEButton);
        EventManager.mInstance.AddEvent(EventManager.EventTypes.END,DisableEButton);

        Cursor.visible = false;
    }

    private void DisableEButton(EventManager.EventTypes status,GameObject obj) {
        isIntButtReq = false;
    }

    private void EnableEButton(EventManager.EventTypes status,GameObject obj) {
        isIntButtReq = true;
    }

    private void Update() {
        switch(mActGameState) {
        case GameMan.GameState.PLAY:
        UpdateKeyboard();
        UpdateMouse();
        break;
        }
    }

    private void UpdateKeyboard() {

        float horiz = Input.GetAxisRaw("Horizontal");

        if(Input.GetKey(mRight) || Input.GetKey(mLeft))
            mShift.x = horiz;
        else
            mShift.x = 0f;

        if(horiz > 0) {
            RotateCharacter(Vector2.one);
        } else if(horiz < 0) {
            RotateCharacter(mTurnLeft);
        }

        if(isIntButtReq) {
            if(Input.GetKeyDown(KeyCode.E)) {
                EventManager.mInstance.ExcecuteEvent(EventManager.EventTypes.EXCECUTE);
            }
        }

        if(Input.GetKeyDown(KeyCode.Escape)) {
            GameMan.mInstance.LoadScene("Menu");
        }

        if(Input.GetKey(KeyCode.Mouse0)) {
            Debug.Log("End");
            EventManager.mInstance.ExcecuteEvent(EventManager.EventTypes.END);
        }

        mPossesCharAnimat.SetFloat("Speed",Mathf.Abs(horiz));

        //Ruch obiecktow
        mCharPosses.position += mShift * Time.deltaTime * mSpeed;
        mCamera.position = Vector3.Lerp(mCamera.position,mCharPosses.position + mCamOffset,mCamSpeed * Time.deltaTime);
        transform.position = mCharPosses.position;// + mOffset;
    }

    void RotateCharacter(Vector2 dir) {
        mCharPosses.localScale = dir * mCharScale;
    }

    private void UpdateMouse() {

    }

}