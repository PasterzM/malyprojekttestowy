﻿using UnityEngine;
internal class ForceMovingObstacle {
    Vector3 mActForce;
    Vector3 pos;

    Rigidbody2D mObstacle;

    float marginX = 150f;
    float marginY = 1000f;

    public ForceMovingObstacle(Vector3 start,Rigidbody2D obstacle) {
        mActForce = Vector3.zero;
        mObstacle = obstacle;
        pos = start;
    }

    public void Update(Vector3 mousePos) {
        mousePos.x -= Screen.width / 2;
        mousePos.y -= Screen.height / 2;

        //mActForce += (mousePos - pos) * 2f;
        mActForce.x = Mathf.Clamp((mousePos.x + pos.x),-marginX,marginX);
        mActForce.y = Mathf.Clamp((mousePos.y + pos.y),-marginY/2,marginY);
        pos = mousePos;
        mObstacle.AddForce(mActForce * Time.deltaTime);
    }
}