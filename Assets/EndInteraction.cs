using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndInteraction:MonoBehaviour {
    Transform mPlayer;

    private void Start() {
    }

    public void OnTriggerEnter2D(Collider2D collision) {
        if(collision.tag == "Player") {
            mPlayer = collision.transform;
            EventManager.mInstance.ExcecuteEvent(EventManager.EventTypes.ENDGAME);
        }
    }

    public void OnTriggerExit2D(Collider2D collision) {
        if(collision.tag == "Player")
            mPlayer = null;
    }
}
